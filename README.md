## Torciv RAT

> This is still work in progress..

## Disclaimer

- This tool was built to production and harmfull to even the attackers system, only run the clients code in a _virtual machine_!

- This tool is for educational purposes, am not to be held responsible for any damage in your PC, use and experiment at your own risk!

> tociv is a reverse, shell trojan, capable of encrypting and stealing files from your victims computer, it also provides a reverse shell for the attacker to execute commands

Ethical hacking and ethical hacker are terms used to describe hacking performed by a company or individual to help identify potential threats on a computer or network. An ethical hacker attempts to bypass system security and search for any weak points that could be exploited by malicious hackers.

## Features

- Reverse shell
- Kills antiviruses and run every time at startup in daemon mode
- File encryption / randsome ware
- Detect important files and grabs them to your server
- Steals, decrypts chrome passwords and sends it to attacker via mail or ftp
- Keylogger - coming soon
- cross platform

## Installation

Tociv RAT is entirely written in python3.7 and compiled to machine code using [py installer](https://github.com/pyinstaller/pyinstaller).
PyInstaller bundles a Python application and all its dependencies into a single package. The user can run the packaged app without installing a Python interpreter or any modules, this script cloaks its self as an antivirus and tends to run in a windows host process, making it almost impossible to detect.

Am targeting windows machines but the script was coded in a \*nix enviroment

```sh
$ python3 -m pip install -r requirements.txt
```

The file structure is as follows

```sh
├── client
│   ├── administrator.py
│   ├── client.pyw
│   ├── commands.py
│   ├── encryption.py
│   ├── getPasswords.py
│   ├── helpers.py
│   ├── ico
│   │   ├── noto.ico
│   │   └── windows.ico
│   └── keylogger.py
├── README.md
└── server
    ├── ftpServer.py
    └── server.py
```

More docs coming soon..
